package com.mdashcam;

import android.Manifest;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.hardware.Camera;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import org.apache.cordova.PermissionHelper;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;

import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;

import org.json.JSONArray;
import org.json.JSONException;

//public class CameraPreview extends CordovaPlugin implements CircularCameraActivity.CameraPreviewListener {
public class CircularCamera extends CordovaPlugin {

    private final String TAG = "CircularCamera";
    private final String startCameraAction = "startCamera";
    private final String stopCameraAction = "stopCamera";
    private final String setTextAction = "setText";
    private final String setFilePathAction = "setFilePath";

    public static final int START_CAMERA_SEC = 0;
    public static final int PERMISSION_DENIED_ERROR = 20;

    public CallbackContext callbackContext;

    protected final static String[] permissions = {
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE
    };

    private CircularCameraRecorderActivity fragment;
    private CallbackContext takePictureCallbackContext;
    private int containerViewId = 1;

    public CircularCamera(){
        super();
        Log.d(TAG, "Constructing");
    }

    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        this.callbackContext = callbackContext;

        if (startCameraAction.equals(action)){
            return callStartCamera(args, callbackContext);
        }
        else if (stopCameraAction.equals(action)){
            return stopCamera(args, callbackContext);
        }
        else if (setTextAction.equals(action)){
            return setText(args, callbackContext);
        }
        else if (setFilePathAction.equals(action)){
            return setFilePath(args, callbackContext);
        }

        return false;
    }

    public void onRequestPermissionResult(int requestCode, String[] permissions,
                                          int[] grantResults) throws JSONException
    {
        for(int r:grantResults)
        {
            if(r == PackageManager.PERMISSION_DENIED)
            {
                this.callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, PERMISSION_DENIED_ERROR));
                return;
            }
        }
    }

    public boolean callStartCamera(final JSONArray args, CallbackContext callbackContext) {
        boolean readExternalStoragePermission = PermissionHelper.hasPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        boolean writeExternalStoragePermission = PermissionHelper.hasPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        boolean startCameraPermission = PermissionHelper.hasPermission(this, Manifest.permission.CAMERA);

        if(!startCameraPermission) {
            startCameraPermission = true;
            try {
                PackageManager packageManager = this.cordova.getActivity().getPackageManager();
                String[] permissionsInPackage = packageManager.getPackageInfo(this.cordova.getActivity().getPackageName(), PackageManager.GET_PERMISSIONS).requestedPermissions;
                if (permissionsInPackage != null) {
                    for (String permission : permissionsInPackage) {
                        if (permission.equals(Manifest.permission.CAMERA)) {
                            startCameraPermission = false;
                            break;
                        }
                    }
                }
            } catch (NameNotFoundException e) {
                // We are requesting the info for our package, so this should
                // never be caught
            }
        }

        if (startCameraPermission && readExternalStoragePermission && writeExternalStoragePermission) {
            return startCamera(args, callbackContext);
        } else if (readExternalStoragePermission && writeExternalStoragePermission && !startCameraPermission) {
            PermissionHelper.requestPermission(this, START_CAMERA_SEC, Manifest.permission.CAMERA);
        } else if (startCameraPermission && writeExternalStoragePermission && !readExternalStoragePermission) {
            PermissionHelper.requestPermission(this, START_CAMERA_SEC, Manifest.permission.READ_EXTERNAL_STORAGE);
        } else if (startCameraPermission && readExternalStoragePermission && !writeExternalStoragePermission) {
            PermissionHelper.requestPermission(this, START_CAMERA_SEC, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        } else {
            PermissionHelper.requestPermissions(this, START_CAMERA_SEC, permissions);
        }

        return false;
    }

    private boolean startCamera(final JSONArray args, CallbackContext callbackContext) {
        Log.d(TAG, "startCamera XXX");
        if(fragment != null){
            return false;
        }
        fragment = new CircularCameraRecorderActivity();
        fragment.setEventListener(this);

        cordova.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                try {
                    DisplayMetrics metrics = cordova.getActivity().getResources().getDisplayMetrics();
                    int x = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, args.getInt(0), metrics);
                    int y = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, args.getInt(1), metrics);
                    int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, args.getInt(2), metrics);
                    int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, args.getInt(3), metrics);
                    String defaultCamera = args.getString(4);
                    Boolean tapToTakePicture = args.getBoolean(5);
                    Boolean dragEnabled = args.getBoolean(6);
                    Boolean toBack = args.getBoolean(7);

                    fragment.defaultCamera = defaultCamera;
                    fragment.tapToTakePicture = tapToTakePicture;
                    fragment.dragEnabled = dragEnabled;
                    fragment.setRect(x, y, width, height);

                    //create or update the layout params for the container view
                    FrameLayout containerView = (FrameLayout)cordova.getActivity().findViewById(containerViewId);
                    if(containerView == null){
                        containerView = new FrameLayout(cordova.getActivity().getApplicationContext());
                        containerView.setId(containerViewId);

                        FrameLayout.LayoutParams containerLayoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
                        cordova.getActivity().addContentView(containerView, containerLayoutParams);
                    }
                    //display camera bellow the webview
                    if(toBack){
                        webView.getView().setBackgroundColor(0x00000000);
                        ((ViewGroup)webView.getView()).bringToFront();
                    }
                    else{
                        //set camera back to front
                        containerView.setAlpha(Float.parseFloat(args.getString(8)));
                        containerView.bringToFront();
                    }

                    //add the fragment to the container
                    FragmentManager fragmentManager = cordova.getActivity().getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.add(containerView.getId(), fragment);
                    fragmentTransaction.commit();
                }
                catch(Exception e){
                    e.printStackTrace();
                }
            }
        });

        return true;
    }

    private boolean stopCamera(final JSONArray args, CallbackContext callbackContext) {
        Log.d(TAG, "stopCamera XXX");
        if(fragment == null){
            return false;
        }

        FragmentManager fragmentManager = cordova.getActivity().getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.remove(fragment);
        fragmentTransaction.commit();
        fragment = null;

        return true;
    }

    private boolean setText(final JSONArray args, CallbackContext callbackContext) {
        Log.d(TAG, "setText XXX");
        if (fragment == null) return false;

        try {
            String text = args.getString(0);
            fragment.setText(text);
        }
        catch(Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    private boolean setFilePath(final JSONArray args, CallbackContext callbackContext) {
        Log.d(TAG, "setFilePath XXX");
        if (fragment == null) return false;

        try {
            String filePath = args.getString(0);
            fragment.setFilePath(filePath);
        }
        catch(Exception e) {
            e.printStackTrace();
        }

        return true;
    }
}
