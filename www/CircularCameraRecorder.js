var argscheck = require('cordova/argscheck')
  , utils = require('cordova/utils')
  , exec = require('cordova/exec');

var PLUGIN_NAME = "CircularCameraRecorder";

var CircularCameraRecorder = function() {};

//@param rect {x: 0, y: 0, width: 100, height:100}
//@param defaultCamera "front" | "back"
CircularCameraRecorder.startCamera = function(rect, defaultCamera, tapEnabled, dragEnabled, toBack, alpha) {
    if (typeof(alpha) === 'undefined') alpha = 1;
    exec(null, null, PLUGIN_NAME, "startCamera", [rect.x, rect.y, rect.width, rect.height, defaultCamera, !!tapEnabled, !!dragEnabled, !!toBack, alpha]);
};

CircularCameraRecorder.stopCamera = function() {
    exec(null, null, PLUGIN_NAME, "stopCamera", []);
};

CircularCameraRecorder.setText = function(text) {
    var params = [""];
    if (text) params = [text];
    exec(null, null, PLUGIN_NAME, "setText", params);
};

CircularCameraRecorder.setFilePath = function(filePath) {
    var params = [""];
    if (filePath) params = [filePath];
    exec(null, null, PLUGIN_NAME, "setFilePath", params);
};


module.exports = CircularCameraRecorder;
